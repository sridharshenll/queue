<?php
include("includes/header.php");
?>
		<div id="body_container" style="">
			<div id="body_right">
				<div id="fixedscroll">
					<script src="js/json2.js"></script>
					<script src="js/dumbFormState-1.js"></script>
					<?php
						include('includes/left_menu.php');
					?>
				</div>
			</div>
			<div id="body_left">
			
				<h2 style="text-align:left; margin-left:25px;"><br>Links<br><br></h2>
				<div id="page_content">
						<p><a href="http://www.racgp.org.au/" target="_Blank">The Royal Australian College of General Practitioners</a><br>
						<p><a href="http://www.medicareaustralia.gov.au/" target="_Blank">Medicare Australia</a><br>
						<p><a href="http://www.uwa.edu.au/" target="_Blank"> The University of Western Australia</a><br>
						<p><a href="http://www.medicalobserver.com.au/" target="_Blank">Medical Observer - for Australian General Practitioners</a><br>
						<p><a href="http://www.australiandoctor.com.au/" target="_Blank">Australian Doctor</a><br>
						<p><a href="http://www.gpet.com.au/" target="_Blank">Australian General Practice Training</a><br>
						<p><a href="http://www.wadems.org.au/" target="_Blank">Western Australian Deputising Medical Service</a><br>
						
						<p style="color:white">If you are experiencing any chest pain, difficulty with breathing or any acute symptoms associated with the heart, lungs, or the brain, please go to your nearest hospital or call the St John Ambulance on 000 immediately. Do not wait in the waiting room.
				</div>
	

﻿			</div>
		</div>
<?php
include("includes/footer.php");
?>