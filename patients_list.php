<?php
include("includes/configure.php");
$location=$_GET["loc"];
$FromLocation=$_GET["frm"];

include("includes/header.php");

?>


<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.21/jquery-ui.min.js"></script>
<script src="js/jquery.ui.touch-punch.min.js" language="javascript"></script>

<style>
#content{
	background: #fff;
	margin-left: 0px;
	 overflow: visible;
	padding-bottom: 30px;
	min-height: 100%;
}
.navbar .container .navbar-brand {
	display:block;
}
@media screen and (min-width: 1000px) and (max-width: 1920px) {
  .page-header {
    margin-top:125px;
  }
}
@media screen and (max-width: 1000px) {
  .page-header {
    margin-top:50px;
  }
}

@media (min-width: 1921px) {
  .page-title > h3 {
   font-size: 45px;
  }
  .widget,.box{
	  font-size: 40px;
  }
  .icon-reorder{
	   font-size: 40px !important;
  }
  .patientfon{
	   font-size: 40px !important;
  }
}
</style>

<script>
url = document.location.href;
xend = url.lastIndexOf("/") + 1;
var base_url = url.substring(0, xend);

$(function() {
$( "#sortable" ).sortable({
	revert: true,
 axis: 'y',
    update: function (event, ui) {
        var data = $(this).sortable('serialize');
        // POST to server using $.post or $.ajax
        $.ajax({
            data: data,
            type: 'POST',
            url: base_url + 'includes/savequeue.php',
			success: function(strRplytxt){
		   }
        });
    }});
$( "#draggable" ).draggable({
connectToSortable: "#sortable",
helper: "clone",
revert: "invalid"
});
$( "ul, li" ).disableSelection();
});

var currenttime = '<?php print date("F d, Y H:i:s", time())?>' //PHP method of getting server date

var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
var serverdate=new Date(currenttime)

function padlength(what){
var output=(what.toString().length==1)? "0"+what : what
return output
}

function displaytime(){
serverdate.setSeconds(serverdate.getSeconds()+1)
var datestring=montharray[serverdate.getMonth()]+" "+padlength(serverdate.getDate())+", "+serverdate.getFullYear()
//var timestring=padlength(serverdate.getHours())+":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds())

 var hours = serverdate.getHours();
 var minutes = serverdate.getMinutes();

var ampm = hours >= 12 ? 'pm' : 'am';

    if (hours > 12) {
        hours -= 12;
    } else if (hours === 0) {
        hours = 12;
    }
var timestring= padlength(hours) +":"+padlength(serverdate.getMinutes())+":"+padlength(serverdate.getSeconds());


document.getElementById("servertime").innerHTML=datestring+" "+timestring +" "+ampm;
}

window.onload=function(){
setInterval("displaytime()", 1000)
}
</script>

		<!-- Center Main page Content -->
		<div id="content"  style="margin-left:0px;">
			<div class="container">
				<!--=== Page Header ===-->

			<div class="page-header">
					<div class="page-title">
						<h3>Current Queue in  <?php echo $location;?></h3>
					</div>		
					
				</div>
				<!-- /Page Header -->

				 
				<!--=== Responsive DataTable ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header" style="line-height:inherit;">
								<h4 class="patientfon"><i class="icon-reorder"></i>Patients</h4>
								<div style="float:right;"><strong>Current Time: <span id="servertime"></span></strong></div>
								<!-- <div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div> -->
							</div>
												
							<div class="widget-content">
								<table class="table table-hover table-striped table-bordered table-highlight-head">
									<thead>
										<tr>
											<th width="40%">Doctor</th>
											<th width="30%">Patient (initials)</th>
											<th width="30%">Time (waiting since)</th>
										</tr>
									</thead>
									 <tbody>
									<!--  <tr>
									 <td colspan='3'>
									 <ul id="sortable" style="margin:0px;border:0px solid red;text-indent:3px;padding-left:2px">  -->
									 <?php
										$getPatientsQry="select * from tbl_patient where location='".$location."' and patient_status='Appointment fixed' and register_date='".date('Y-m-d')."' order by display_order,reg_time asc";
									 //exit;
									 $getPatientsRes=mysql_query($getPatientsQry);
									 $getPatientsCnt=mysql_num_rows($getPatientsRes);
									 if($getPatientsCnt>0){
										 while($getPatientsRow=mysql_fetch_array($getPatientsRes)){
											 $doctor_id=$getPatientsRow["doctor_id"];
											 if($doctor_id>0&&$doctor_id!=""){
												 $getDocQry="select * from tbl_staff where staff_id='".$doctor_id."'";
												 $getDocRes=mysql_query($getDocQry);
												 $getDocRow=mysql_fetch_array($getDocRes);
												 $doctor_name=stripslashes($getDocRow["staff_name"]);
											 }
											 else{
                                               $doctor_name="First Available Doctor";
											 }
											 $reg_time=stripslashes($getPatientsRow["reg_time"]);
											 if($reg_time!="" && $reg_time!="00:00:00"){
											  $reg_time=date('g:i A',strtotime(stripslashes($getPatientsRow["reg_time"])));
											}
										 ?>
												
											<!-- <li style="list-style-type:none;margin:0px;" id="item-<?php echo $getPatientsRow["patient_id"]?>"> 
												<table class="table table-striped table-bordered" width="100%" style="margin:0px;padding-left:-25px"> -->
													<tr>
														<td width="50%"><?php echo $doctor_name;?></td>
														<td width="25%"><?php echo strtoupper(substr(stripslashes($getPatientsRow["patient_name"]),0,1).substr(stripslashes($getPatientsRow["family_name"]),0,1))?></td>
														<td width="25%" nowrap><?php echo  $reg_time;?></td>											
													</tr>
												<!-- </table>
											</li> -->
											
										<?php
										 }
										?>
							<!-- 			</td>
										</ul>
										</tr> -->
										<?php
									    }
									
										else{

										?>
										<tr>
											<td colspan="3"><center>No patients found.</center></td>
										</tr>
										<?php
										}
										?>
										
									</tbody>
								</table>
								<!-- <div class="row">
									<div class="table-footer">										
										<div class="col-md-12">
											<ul class="pagination">
												<li class="disabled"><a href="javascript:void(0);">&larr; Prev</a></li>
												<li class="active"><a href="javascript:void(0);">1</a></li>
												<li><a href="javascript:void(0);">2</a></li>
												<li><a href="javascript:void(0);">3</a></li>
												<li><a href="javascript:void(0);">4</a></li>
												<li><a href="javascript:void(0);">Next &rarr;</a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
						</div>
						<?php
						//if(!isset($_GET["frm"]))
						//{
						?>
						<h5>Please <a href="index.php">click here </a>to go in the queue</h5>
						<?php
						//}
						?>
					</div>

					<!-- /Table with Footer -->
							
						</div>
					</div>
				</div>
				<!-- /Responsive DataTable -->
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>

		<!-- /Center Main page Content -->
<?php
include("includes/footer.php");
?>

