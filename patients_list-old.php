<?php
include("includes/configure.php");
$location=$_GET["loc"];
include("includes/header.php");
?>
<style>
#content{
	background: #fff;
	margin-left: 0px;
	 overflow: visible;
	padding-bottom: 30px;
	min-height: 100%;
}
.navbar .container .navbar-brand {
	display:block;
}
</style>
		<!-- Center Main page Content -->
		<div id="content"  style="margin-left:0px;">
			<div class="container">
				<!--=== Page Header ===-->
				<div class="page-header" style="margin-top:50px;">
					<div class="page-title">
						<h3>Current Queue in <?php echo $location;?></h3>
					</div>					
				</div>
				<!-- /Page Header -->

				
				<!--=== Responsive DataTable ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Patients</h4>
								<!-- <div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div> -->
							</div>
												
							<div class="widget-content">
								<table class="table table-hover table-striped table-bordered table-highlight-head">
									<thead>
										<tr>
											
											<th>Doctor</th>
											<th>Patient (initials)</th>
											<th>Time (waiting since)</th>
											
										</tr>
									</thead>
									 <tbody>
									 <?php
										$getPatientsQry="select * from tbl_patient where location='".$location."' and patient_status='Apointment fixed' and register_date='".date('Y-m-d')."' order by reg_time asc";
									 //exit;
									 $getPatientsRes=mysql_query($getPatientsQry);
									 $getPatientsCnt=mysql_num_rows($getPatientsRes);
									 if($getPatientsCnt>0){
										 while($getPatientsRow=mysql_fetch_array($getPatientsRes)){
											 $doctor_id=$getPatientsRow["doctor_id"];
											 if($doctor_id>0&&$doctor_id!=""){
												 $getDocQry="select * from tbl_staff where staff_id='".$doctor_id."'";
												 $getDocRes=mysql_query($getDocQry);
												 $getDocRow=mysql_fetch_array($getDocRes);
												 $doctor_name=stripslashes($getDocRow["staff_name"]);
											 }
											 else{
                                               $doctor_name="First Avialble Doctor";
											 }
											 $reg_time=stripslashes($getPatientsRow["reg_time"]);
											 if($reg_time!="" && $reg_time!="00:00:00"){
											  $reg_time=date('g:i A',strtotime(stripslashes($getPatientsRow["reg_time"])));
											}
										 ?>
										<tr>

											<td><?php echo $doctor_name;?></td>
											<td><?php echo substr(stripslashes($getPatientsRow["patient_name"]),0,1).substr(stripslashes($getPatientsRow["family_name"]),0,1);?></td>
											<td><?php echo  $reg_time;?></td>											
											
										</tr>		
											
										<?php
										 }
									    }else{

										?>
										<tr>
											<td colspan="3"><center>No patients found.</center></td>
										</tr>
										<?php
										}
										?>
									</tbody>
								</table>
								<!-- <div class="row">
									<div class="table-footer">										
										<div class="col-md-12">
											<ul class="pagination">
												<li class="disabled"><a href="javascript:void(0);">&larr; Prev</a></li>
												<li class="active"><a href="javascript:void(0);">1</a></li>
												<li><a href="javascript:void(0);">2</a></li>
												<li><a href="javascript:void(0);">3</a></li>
												<li><a href="javascript:void(0);">4</a></li>
												<li><a href="javascript:void(0);">Next &rarr;</a></li>
											</ul>
										</div>
									</div>
								</div> -->
							</div>
						</div>
						<h5>Please <a href="index.php">click here</a> to add in the queue</h5>
					</div>

					<!-- /Table with Footer -->
							
						</div>
					</div>
				</div>
				<!-- /Responsive DataTable -->
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>

		<!-- /Center Main page Content -->
<?php
include("includes/footer.php");
?>