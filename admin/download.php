<?php 
// Modify for download
include('../includes/configure.php');
include_once('../api/Common.php');
include("includes/session_check.php");

error_reporting(0);
$WalkingpAPPApi = new Common($DBCONN);

$patient_id=$doctor_id=$name=$given_name=$family_name=$dob=$reference_number=$expiry_date=$address=$suburb=$post_code=$home_phone=$mobile_phone = $pension_card_no=$pension_card_expiry_date=$medicare_no=$patient_title ="";
if (isset($_GET['id'])) {
	$Id = $_GET['id'];

	$getResCnt="";
	$Qry = "SELECT * FROM tbl_patient where patient_id =:patient_id";
	$qryParams['patient_id'] = $Id;
	$getResCnt = $WalkingpAPPApi->funBckendExeSelectQuery($Qry,$qryParams);
	if (count($getResCnt)>0) {
    	foreach ($getResCnt as $PatientListData) {
        	$patient_id = $PatientListData['patient_id'];
        	$patient_title = stripslashes($PatientListData['title']);
        	//Title and titleCode mapping
        	if ($patient_title == 'Mr')
        		$titleCodeMapping = 1;
        	if ($patient_title == 'Mrs')
        		$titleCodeMapping = 2;
        	if ($patient_title == 'Ms')
        		$titleCodeMapping = 3;
        	if ($patient_title == 'Miss')
        		$titleCodeMapping = 4;
        	if ($patient_title == 'Master')
        		$titleCodeMapping = 5;
        	//end of Title and titleCode mapping

        	$doctor_id = $PatientListData['doctor_id'];
        	$given_name = stripslashes($PatientListData['patient_name']);
        	$family_name = stripslashes($PatientListData['family_name']);
        	$dob = $PatientListData['dob'];
        	$email = stripslashes($PatientListData['email_address']);
        	$reference_number = ($PatientListData['reference_no']=="0")?"":$PatientListData['reference_no'];
       		$medicare_no = stripslashes($PatientListData['medicare_no']);
        	if (!empty($medicare_no)) {
             	$expiry_date = $PatientListData['expiry_date'];
        	} else {
           		$expiry_date = "";
           		$reference_number = "";
        	}
        	$pension_card_no = stripslashes($PatientListData['pension_card_no']);
            $address = stripslashes($PatientListData['address']);
            $suburb = stripslashes($PatientListData['suburb']);
            $post_code = stripslashes($PatientListData['post_code']);
            $home_phone = stripslashes($PatientListData['home_phone']);
            $mobile_phone = stripslashes($PatientListData['mobile_phone']);
            $pension_card_expiry_date = $PatientListData['pension_card_expiry_date'];
            if ($PatientListData["dob"]!="") {
                list($year,$month,$day)=explode("-",$PatientListData["dob"]);
                if (strlen($day)==1) {
                    $day = "0".$day;
                }

                //modify dob format
                if (strlen($month)==1) {
                	$month = "0".$month;
                }

                $dobFormatArray = array($day, $month, $year);
                $dob = join('/', $dobFormatArray);
            }
            $Expiry_date=stripslashes($PatientListData["expiry_date"]);
            if ($medicare_no!="") {
                if (strpos($Expiry_date,"-")!==false) {
                    list($expmonth,$expyear)=explode("-",$Expiry_date);
                } else {
                    list($expmonth,$expyear)=explode("/",$Expiry_date);
                }
            }
            //modify expiry date format
            $expiryFormatArray = array($expmonth, $expyear);
            $Expiry_date= join('/', $expiryFormatArray);
            // end of modify expiry date format
            $Pension_card_expiry_date = stripslashes($PatientListData["pension_card_expiry_date"]);
            if (stripslashes($pension_card_no)!="")
                list($hccyear,$hccmonth,$hccday)=explode("-",$PatientListData["pension_card_expiry_date"]);
                if(strlen($hccday)==1) {
                    $hccday = "0".$hccday;
                }

              	//modify pension card expiry date format
                if (strlen($hccmonth)==1) {
                	$hccmonth = "0".$hccmonth;
                }

                $hccFormatArray = array($hccday, $hccmonth, $hccyear);
                $Pension_card_expiry_date= join('/', $hccFormatArray);
              	//end of modify pension card expiry date format
        }
        $next_of_kin     = ($PatientListData["next_of_kin"]!="")?stripslashes($PatientListData["next_of_kin"]):"";
        $contact_number = ($PatientListData["contact_number"]!="")?$PatientListData["contact_number"]:"";
    }
}
// Modify for download 
$filename = trim($given_name).'.xml';
header('Content-Disposition: attachment; filename=' . $filename);
header("Content-Type: application/force-download");
header('Pragma: private');
header('Cache-control: private, must-revalidate');

$xml = new DomDocument('1.0', "ISO-8859-1");
$xml->formatOutput=true;

$BPSEHRV2 = $xml->createElement("BPSEHRV2");
$xml -> appendChild($BPSEHRV2);

$BPSVersion = $xml->createElement("BPSVersion", "1.10.3.908");
$BPSEHRV2->appendChild($BPSVersion);

$Practice = $xml->createElement("Practice");
$BPSEHRV2->appendChild($Practice);

$PracticeName = $xml->createElement("PracticeName", "Doctorai");
$Practice->appendChild($PracticeName);

$PracticeID = $xml->createElement("PracticeID", 23734);
$Practice->appendChild($PracticeID);

$Demographics = $xml->createElement("Demographics");
$BPSEHRV2->appendChild($Demographics);

$Patient = $xml->createElement("Patient");
$Demographics->appendChild($Patient);

$INTERNALID = $xml->createElement("INTERNALID");
$Patient->appendChild($INTERNALID);	

$RECORDSTATUS = $xml->createElement("RECORDSTATUS", 1);
$Patient->appendChild($RECORDSTATUS);

$EXTERNALID = $xml->createElement("EXTERNALID", "NIL");
$Patient->appendChild($EXTERNALID);

$IHI = $xml->createElement("IHI");
$Patient->appendChild($IHI);	

$IHISTATUS = $xml->createElement("IHISTATUS", 0);
$Patient->appendChild($IHISTATUS);

$IHIRECORDSTATUS = $xml->createElement("IHIRECORDSTATUS", 0);
$Patient->appendChild($IHIRECORDSTATUS);	

$IHISOURCE = $xml->createElement("IHISOURCE", 0);
$Patient->appendChild($IHISOURCE);	

$ORIGIN = $xml->createElement("ORIGIN", 1);
$Patient->appendChild($ORIGIN);		

$PATIENTSTATUS = $xml->createElement("PATIENTSTATUS", 1);//need to check this field
$Patient->appendChild($PATIENTSTATUS);

$DATEOFDEATH = $xml->createElement("DATEOFDEATH", "NIL");
$Patient->appendChild($DATEOFDEATH);

$TITLECODE = $xml->createElement("TITLECODE", $titleCodeMapping);//need to check this field
$Patient->appendChild($TITLECODE);

$FIRSTNAME = $xml->createElement("FIRSTNAME", $given_name); //according to patientlist file
$Patient->appendChild($FIRSTNAME);

$MIDDLENAME = $xml->createElement("MIDDLENAME");
$Patient->appendChild($MIDDLENAME);

$SURNAME = $xml->createElement("SURNAME", $family_name); //according to patientlist file
$Patient->appendChild($SURNAME);

$PREFERREDNAME = $xml->createElement("PREFERREDNAME");
$Patient->appendChild($PREFERREDNAME);

$ADDRESS1 = $xml->createElement("ADDRESS1", $address); //according to patientlist file
$Patient->appendChild($ADDRESS1);

$ADDRESS2 = $xml->createElement("ADDRESS2");
$Patient->appendChild($ADDRESS2);

$CITY = $xml->createElement("CITY", $suburb); //according to patientlist file
$Patient->appendChild($CITY);

$POSTCODE = $xml->createElement("POSTCODE", $post_code); //according to patientlist file
$Patient->appendChild($POSTCODE);

$POSTALADDRESS = $xml->createElement("POSTALADDRESS");
$Patient->appendChild($POSTALADDRESS);

$POSTALCITY = $xml->createElement("POSTALCITY");
$Patient->appendChild($POSTALCITY);

$POSTALPOSTCODE = $xml->createElement("POSTALPOSTCODE");
$Patient->appendChild($POSTALPOSTCODE);

$DOB = $xml->createElement("DOB", $dob); //according to patientlist file, format "16/08/1980"
$Patient->appendChild($DOB);

//$SEXCODE = $xml->createElement("SEXCODE", 2);
$SEXCODE = $xml->createElement("SEXCODE");
$Patient->appendChild($SEXCODE);

$ETHNICCODE = $xml->createElement("ETHNICCODE", 0);
$Patient->appendChild($ETHNICCODE);

$HOMEPHONE = $xml->createElement("HOMEPHONE", $home_phone); //according to patientlist file
$Patient->appendChild($HOMEPHONE);

$WORKPHONE = $xml->createElement("WORKPHONE");
$Patient->appendChild($WORKPHONE);

$MOBILEPHONE = $xml->createElement("MOBILEPHONE", $mobile_phone); //according to patientlist file
$Patient->appendChild($MOBILEPHONE);

$MEDICARENO = $xml->createElement("MEDICARENO", $medicare_no); //according to patientlist file
$Patient->appendChild($MEDICARENO);

$MEDICARELINENO = $xml->createElement("MEDICARELINENO", $reference_number); //according to patientlist file
$Patient->appendChild($MEDICARELINENO);

$MEDICAREEXPIRY = $xml->createElement("MEDICAREEXPIRY", $Expiry_date);//according to patientlist file, format "09/2024"
$Patient->appendChild($MEDICAREEXPIRY);

$PENSIONCODE = $xml->createElement("PENSIONCODE", 0);
$Patient->appendChild($PENSIONCODE);

$PENSIONNO = $xml->createElement("PENSIONNO",$pension_card_no); //according to patientlist file
$Patient->appendChild($PENSIONNO);

$PENSIONSTART = $xml->createElement("PENSIONSTART","NIL");
$Patient->appendChild($PENSIONSTART);

$PENSIONEXPIRY = $xml->createElement("PENSIONEXPIRY", $Pension_card_expiry_date);//according to patientlist file, format "31/08/2020"
$Patient->appendChild($PENSIONEXPIRY);

$DVACODE = $xml->createElement("DVACODE", 0);
$Patient->appendChild($DVACODE);

$DVANO = $xml->createElement("DVANO");
$Patient->appendChild($DVANO);

$SAFETYNETNO = $xml->createElement("SAFETYNETNO");
$Patient->appendChild($SAFETYNETNO);

$RECORDNO = $xml->createElement("RECORDNO");
$Patient->appendChild($RECORDNO);

$RELIGION = $xml->createElement("RELIGION");
$Patient->appendChild($RELIGION);

$HEALTHFUNDNO = $xml->createElement("HEALTHFUNDNO");
$Patient->appendChild($HEALTHFUNDNO);

$HEALTHFUNDNAME = $xml->createElement("HEALTHFUNDNAME");
$Patient->appendChild($HEALTHFUNDNAME);

$HEALTHFUNDID = $xml->createElement("HEALTHFUNDID", 0);
$Patient->appendChild($HEALTHFUNDID);

$HEALTHFUNDEXPIRY = $xml->createElement("HEALTHFUNDEXPIRY","NIL");
$Patient->appendChild($HEALTHFUNDEXPIRY);

$ACCOUNTCODE = $xml->createElement("ACCOUNTCODE", 3);
$Patient->appendChild($ACCOUNTCODE);

$USERID = $xml->createElement("USERID",0);
$Patient->appendChild($USERID);

$DENYACCESS = $xml->createElement("DENYACCESS",0);
$Patient->appendChild($DENYACCESS);

$NOREMINDERS = $xml->createElement("NOREMINDERS",0);
$Patient->appendChild($NOREMINDERS);

$CONTACTMETHOD = $xml->createElement("CONTACTMETHOD",0);
$Patient->appendChild($CONTACTMETHOD);

$HEADOFFAMILYID = $xml->createElement("HEADOFFAMILYID"); //check this value
$Patient->appendChild($HEADOFFAMILYID);

$NEXTOFKINID = $xml->createElement("NEXTOFKINID");//modify for NOK working
$Patient->appendChild($NEXTOFKINID);

$EMERGENCYID = $xml->createElement("EMERGENCYID", 0);
$Patient->appendChild($EMERGENCYID);

$REFERRINGDOCTOR = $xml->createElement("REFERRINGDOCTOR","NIL");
$Patient->appendChild($REFERRINGDOCTOR);

$REFERRALDATE = $xml->createElement("REFERRALDATE","NIL");
$Patient->appendChild($REFERRALDATE);

$REFERRALCODE = $xml->createElement("REFERRALCODE",0);
$Patient->appendChild($REFERRALCODE);

$ERX = $xml->createElement("ERX", 0);
$Patient->appendChild($ERX);

$DISPENSE = $xml->createElement("DISPENSE", 0);
$Patient->appendChild($DISPENSE);

$MEDVIEW = $xml->createElement("MEDVIEW", 0);
$Patient->appendChild($MEDVIEW);

$PHARMACYID = $xml->createElement("PHARMACYID", 0);
$Patient->appendChild($PHARMACYID);

$CTG = $xml->createElement("CTG", 0);
$Patient->appendChild($CTG);

$OTHERNOTES = $xml->createElement("OTHERNOTES");
$Patient->appendChild($OTHERNOTES);

$CONSENTSMSREMINDER = $xml->createElement("CONSENTSMSREMINDER", 0);
$Patient->appendChild($CONSENTSMSREMINDER);

$CREATED = $xml->createElement("CREATED", date("d/m/Y",time()));
$Patient->appendChild($CREATED);

$CREATEDBY = $xml->createElement("CREATEDBY");
$Patient->appendChild($CREATEDBY);

$UPDATED = $xml->createElement("UPDATED", "NIL");
$Patient->appendChild($UPDATED);

$UPDATEDBY = $xml->createElement("UPDATEDBY", "NIL");
$Patient->appendChild($UPDATEDBY);

$COUNTRY = $xml->createElement("COUNTRY", "NIL");
$Patient->appendChild($COUNTRY);

$POSTALCOUNTRY = $xml->createElement("POSTALCOUNTRY", "NIL");
$Patient->appendChild($POSTALCOUNTRY);

$IHIVALIDATED = $xml->createElement("IHIVALIDATED", "NIL");
$Patient->appendChild($IHIVALIDATED);

$DEFAULTVISITTYPE = $xml->createElement("DEFAULTVISITTYPE", 0);
$Patient->appendChild($DEFAULTVISITTYPE);

$PREVIOUSUSERID = $xml->createElement("PREVIOUSUSERID", "NIL");
$Patient->appendChild($PREVIOUSUSERID);

$REGISTRATIONID = $xml->createElement("REGISTRATIONID", "NIL");
$Patient->appendChild($REGISTRATIONID);

$ENROLMENTID = $xml->createElement("ENROLMENTID", "NIL");
$Patient->appendChild($ENROLMENTID);

$QED = $xml->createElement("QED", "NIL");
$Patient->appendChild($QED);

$ELIGIBLE_PUBLIC_FUNDING = $xml->createElement("ELIGIBLE_PUBLIC_FUNDING", "NIL");
$Patient->appendChild($ELIGIBLE_PUBLIC_FUNDING);

$ENROLMENTCHECK = $xml->createElement("ENROLMENTCHECK", "NIL");
$Patient->appendChild($ENROLMENTCHECK);

$HCHTIER = $xml->createElement("HCHTIER", 0);
$Patient->appendChild($HCHTIER);

$HCHSTARTED = $xml->createElement("HCHSTARTED", "NIL");
$Patient->appendChild($HCHSTARTED);

$HCHCURRENT = $xml->createElement("HCHCURRENT", "NIL");
$Patient->appendChild($HCHCURRENT);

$PENDINGMOBILECHANGE = $xml->createElement("PENDINGMOBILECHANGE", "NIL");
$Patient->appendChild($PENDINGMOBILECHANGE);

$PUBLICFUNDING = $xml->createElement("PUBLICFUNDING", 0);
$Patient->appendChild($PUBLICFUNDING);

$FINDEPENDANT = $xml->createElement("FINDEPENDANT", 0 );
$Patient->appendChild($FINDEPENDANT);

$CSCHOLDER = $xml->createElement("CSCHOLDER", 0);
$Patient->appendChild($CSCHOLDER);

$HUCHOLDER = $xml->createElement("HUCHOLDER", 0);
$Patient->appendChild($HUCHOLDER);

$CSCNUMBER = $xml->createElement("CSCNUMBER", "NIL");
$Patient->appendChild($CSCNUMBER);

$INCSUPPNUMBER = $xml->createElement("INCSUPPNUMBER", "NIL");
$Patient->appendChild($INCSUPPNUMBER);

$HUCNUMBER = $xml->createElement("HUCNUMBER", "NIL");
$Patient->appendChild($HUCNUMBER);

$CSCVALID = $xml->createElement("CSCVALID", "NIL");
$Patient->appendChild($CSCVALID);

$CSCEXPIRY = $xml->createElement("CSCEXPIRY", "NIL");
$Patient->appendChild($CSCEXPIRY);

$HUCVALID = $xml->createElement("HUCVALID", "NIL");
$Patient->appendChild($HUCVALID);

$HUCEXPIRY = $xml->createElement("HUCEXPIRY", "NIL");
$Patient->appendChild($HUCEXPIRY);

$IWI = $xml->createElement("IWI", 0);
$Patient->appendChild($IWI);

$OVERALL_PATIENTVERSION = $xml->createElement("OVERALL_PATIENTVERSION", "NIL");
$Patient->appendChild($OVERALL_PATIENTVERSION);

$CORE_VERSION = $xml->createElement("CORE_VERSION", "NIL");
$Patient->appendChild($CORE_VERSION);

$MOH_CHECKED = $xml->createElement("MOH_CHECKED", "NIL");
$Patient->appendChild($MOH_CHECKED);

$COUNTRYOFBIRTH = $xml->createElement("COUNTRYOFBIRTH", "NIL");
$Patient->appendChild($COUNTRYOFBIRTH);

$COUNTRYOFBIRTH_SOURCE = $xml->createElement("COUNTRYOFBIRTH_SOURCE", "NIL");
$Patient->appendChild($COUNTRYOFBIRTH_SOURCE);

$PLACEOFBIRTH = $xml->createElement("PLACEOFBIRTH", "NIL");
$Patient->appendChild($PLACEOFBIRTH);

$CITIZEN = $xml->createElement("CITIZEN", "NIL");
$Patient->appendChild($CITIZEN);

$CITIZEN_SOURCE = $xml->createElement("CITIZEN_SOURCE", "NIL");
$Patient->appendChild($CITIZEN_SOURCE);

$DATEOFDEATHSOURCE = $xml->createElement("DATEOFDEATHSOURCE", "NIL");
$Patient->appendChild($DATEOFDEATHSOURCE);

$NAME_SOURCE = $xml->createElement("NAME_SOURCE", "NIL");
$Patient->appendChild($NAME_SOURCE);

$NAME_SETID = $xml->createElement("NAME_SETID", "NIL");
$Patient->appendChild($NAME_SETID);

$NAME_VERSION = $xml->createElement("NAME_VERSION", "NIL");
$Patient->appendChild($NAME_VERSION);

$NAME_TYPE = $xml->createElement("NAME_TYPE", "NIL");
$Patient->appendChild($NAME_TYPE);

$ADDRESS_GEOTAG = $xml->createElement("ADDRESS_GEOTAG", "NIL");
$Patient->appendChild($ADDRESS_GEOTAG);

$ADDRESS_SETID = $xml->createElement("ADDRESS_SETID", "NIL");
$Patient->appendChild($ADDRESS_SETID);

$ADDRESS_VERSION = $xml->createElement("ADDRESS_VERSION", "NIL");
$Patient->appendChild($ADDRESS_VERSION);

$ADDRESS_DOMICILE = $xml->createElement("ADDRESS_DOMICILE", "NIL");
$Patient->appendChild($ADDRESS_DOMICILE);

$ADDRESS_NOTVALID_REASON = $xml->createElement("ADDRESS_NOTVALID_REASON", "NIL");
$Patient->appendChild($ADDRESS_NOTVALID_REASON);

$ESAM_CHECKED = $xml->createElement("ESAM_CHECKED", "NIL");
$Patient->appendChild($ESAM_CHECKED);

$POSTAL_SUBURB = $xml->createElement("POSTAL_SUBURB", "NIL");
$Patient->appendChild($POSTAL_SUBURB);

$POSTAL_ADDRESS_GEOTAG = $xml->createElement("POSTAL_ADDRESS_GEOTAG", "NIL");
$Patient->appendChild($POSTAL_ADDRESS_GEOTAG);

$POSTAL_ADDRESS_DOMICILE = $xml->createElement("POSTAL_ADDRESS_DOMICILE", "NIL");
$Patient->appendChild($POSTAL_ADDRESS_DOMICILE);

$POSTAL_ADDRESS_NOTVALID_REASON = $xml->createElement("POSTAL_ADDRESS_NOTVALID_REASON", "NIL");
$Patient->appendChild($POSTAL_ADDRESS_NOTVALID_REASON);

$POSTAL_ESAM_CHECKED = $xml->createElement("POSTAL_ESAM_CHECKED", "NIL");
$Patient->appendChild($POSTAL_ESAM_CHECKED);

$DOB_SOURCE = $xml->createElement("DOB_SOURCE", "NIL");
$Patient->appendChild($DOB_SOURCE);

$ETHNIC1CODE = $xml->createElement("ETHNIC1CODE", 0);
$Patient->appendChild($ETHNIC1CODE);

$ETHNIC2CODE = $xml->createElement("ETHNIC2CODE", 0);
$Patient->appendChild($ETHNIC2CODE);

$GENDERCODE = $xml->createElement("GENDERCODE", 0);
$Patient->appendChild($GENDERCODE);

$PRONOUNCODE = $xml->createElement("PRONOUNCODE", 0);
$Patient->appendChild($PRONOUNCODE);

$DIFFERENTIDENTITY = $xml->createElement("DIFFERENTIDENTITY");
$Patient->appendChild($DIFFERENTIDENTITY);

$EXTRACTIONOPTOUT = $xml->createElement("EXTRACTIONOPTOUT", 0);
$Patient->appendChild($EXTRACTIONOPTOUT);

$ATRISKOFCOVID = $xml->createElement("ATRISKOFCOVID", 0);
$Patient->appendChild($ATRISKOFCOVID);

$ESCRIPTDEFAULT = $xml->createElement("ESCRIPTDEFAULT", 0);
$Patient->appendChild($ESCRIPTDEFAULT);

$ESCRIPTTOKEN = $xml->createElement("ESCRIPTTOKEN", 0);
$Patient->appendChild($ESCRIPTTOKEN);

$EMAIL = $xml->createElement("EMAIL", $email);
$Patient->appendChild($EMAIL);

$ClinicalDetails = $xml->createElement("ClinicalDetails");
$BPSEHRV2 -> appendChild($ClinicalDetails);

$ClinicalDetails_c = $xml->createElement("ClinicalDetails");
$ClinicalDetails -> appendChild($ClinicalDetails_c);

//$INTERNALID = $xml->createElement("INTERNALID", 468); //check this value
$INTERNALID = $xml->createElement("INTERNALID");
$ClinicalDetails_c -> appendChild($INTERNALID);

$KNOWNALLERGIES = $xml->createElement("KNOWNALLERGIES", 0);
$ClinicalDetails_c -> appendChild($KNOWNALLERGIES);

$PASTHISTORY = $xml->createElement("PASTHISTORY", 0);
$ClinicalDetails_c -> appendChild($PASTHISTORY);

$FAMILYHISTORY = $xml->createElement("FAMILYHISTORY", 0);
$ClinicalDetails_c -> appendChild($FAMILYHISTORY);

$SMOKINGSTATUS = $xml->createElement("SMOKINGSTATUS", 0);
$ClinicalDetails_c -> appendChild($SMOKINGSTATUS);

$ALCOHOLSTATUS = $xml->createElement("ALCOHOLSTATUS", 0);
$ClinicalDetails_c -> appendChild($ALCOHOLSTATUS);

$BLOODGROUP = $xml->createElement("BLOODGROUP", "NIL");
$ClinicalDetails_c -> appendChild($BLOODGROUP);

$RH = $xml->createElement("RH", 0);
$ClinicalDetails_c -> appendChild($RH);

$RETIRED = $xml->createElement("RETIRED", 0);
$ClinicalDetails_c -> appendChild($RETIRED);

$MARITALSTATUS = $xml->createElement("MARITALSTATUS", 0);
$ClinicalDetails_c -> appendChild($MARITALSTATUS);

$SEXUALITY = $xml->createElement("SEXUALITY", 0);
$ClinicalDetails_c -> appendChild($SEXUALITY);

$ELITESPORTS = $xml->createElement("ELITESPORTS", 0);
$ClinicalDetails_c -> appendChild($ELITESPORTS);

$PACEMAKER = $xml->createElement("PACEMAKER", 0);
$ClinicalDetails_c -> appendChild($PACEMAKER);

$SOCIALHX = $xml->createElement("SOCIALHX", "NIL");
$ClinicalDetails_c -> appendChild($SOCIALHX);

$RECREATION = $xml->createElement("RECREATION", "NIL");
$ClinicalDetails_c -> appendChild($RECREATION);

$ACCOMODATION = $xml->createElement("ACCOMODATION", "NIL");
$ClinicalDetails_c -> appendChild($ACCOMODATION);

$LIVESWITH = $xml->createElement("LIVESWITH", "NIL");
$ClinicalDetails_c -> appendChild($LIVESWITH);

$HASCARER = $xml->createElement("HASCARER", "NIL");
$ClinicalDetails_c -> appendChild($HASCARER);

$ISCARER = $xml->createElement("ISCARER", "NIL");
$ClinicalDetails_c -> appendChild($ISCARER);

$WEBSTER = $xml->createElement("WEBSTER", 0);
$ClinicalDetails_c -> appendChild($WEBSTER);

$CAUSEOFDEATH = $xml->createElement("CAUSEOFDEATH");
$ClinicalDetails_c -> appendChild($CAUSEOFDEATH);

$CAUSEOFDEATHCODE = $xml->createElement("CAUSEOFDEATHCODE", 0);
$ClinicalDetails_c -> appendChild($CAUSEOFDEATHCODE);

$OTHERCOMMENT = $xml->createElement("OTHERCOMMENT", "NIL");
$ClinicalDetails_c -> appendChild($OTHERCOMMENT);

$DRNOTE = $xml->createElement("DRNOTE");
$ClinicalDetails_c -> appendChild($DRNOTE);

$HIDEDRNOTE = $xml->createElement("HIDEDRNOTE", 0);
$ClinicalDetails_c -> appendChild($HIDEDRNOTE);

//$CREATED = $xml->createElement("CREATED", "16/08/2020");
$CREATED = $xml->createElement("CREATED", "NIL");
$ClinicalDetails_c -> appendChild($CREATED);

//$CREATEDBY = $xml->createElement("CREATEDBY", 0);
$CREATEDBY = $xml->createElement("CREATEDBY", "NIL");
$ClinicalDetails_c -> appendChild($CREATEDBY);

$UPDATED = $xml->createElement("UPDATED", "NIL");
$ClinicalDetails_c -> appendChild($UPDATED);

$UPDATEDBY = $xml->createElement("UPDATEDBY", "NIL");
$ClinicalDetails_c -> appendChild($UPDATEDBY);

$PCEHR = $xml->createElement("PCEHR", 0);
$ClinicalDetails_c -> appendChild($PCEHR);

$SHS = $xml->createElement("SHS", "NIL");
$ClinicalDetails_c -> appendChild($SHS);

$SAFEINHOME = $xml->createElement("SAFEINHOME", 0);
$ClinicalDetails_c -> appendChild($SAFEINHOME);

$ACD = $xml->createElement("ACD", "NIL");
$ClinicalDetails_c -> appendChild($ACD);

$POWEROFATTORNEY = $xml->createElement("POWEROFATTORNEY", "NIL");
$ClinicalDetails_c -> appendChild($POWEROFATTORNEY);

$NIR = $xml->createElement("NIR", 0);
$ClinicalDetails_c -> appendChild($NIR);

$ObsGynDetail = $xml->createElement("ObsGynDetail");
$BPSEHRV2 -> appendChild($ObsGynDetail);
$PatientConsent = $xml->createElement("PatientConsent");
$BPSEHRV2 -> appendChild($PatientConsent);
$PatientAppEnrolment = $xml->createElement("PatientAppEnrolment");
$BPSEHRV2 -> appendChild($PatientAppEnrolment);

$NextOfKin = $xml->createElement("NextOfKin");
$BPSEHRV2 -> appendChild($NextOfKin);

$NextOfKinRecord = $xml->createElement("NextOfKinRecord");
$NextOfKin -> appendChild($NextOfKinRecord);

$TITLECODE = $xml->createElement("TITLECODE", 0);//modify for NOK working
//$TITLECODE = $xml->createElement("TITLECODE");
$NextOfKinRecord -> appendChild($TITLECODE);

$SURNAME = $xml->createElement("SURNAME");
$NextOfKinRecord -> appendChild($SURNAME);

//$FIRSTNAME = $xml->createElement("FIRSTNAME", "Poppy Mary");
$FIRSTNAME = $xml->createElement("FIRSTNAME", $next_of_kin); //according to patientlist file?
$NextOfKinRecord -> appendChild($FIRSTNAME);

$ADDRESS = $xml->createElement("ADDRESS");
$NextOfKinRecord -> appendChild($ADDRESS);

$CITY = $xml->createElement("CITY");
$NextOfKinRecord -> appendChild($CITY);

$POSTCODE = $xml->createElement("POSTCODE");
$NextOfKinRecord -> appendChild($POSTCODE);

//$PHONE = $xml->createElement("PHONE", "12345678"); //check field type
$PHONE = $xml->createElement("PHONE", $contact_number); //according to patientlist file
$NextOfKinRecord -> appendChild($PHONE);

$PHONE2 = $xml->createElement("PHONE2");
$NextOfKinRecord -> appendChild($PHONE2);

$NEXTOFKINID = $xml->createElement("NEXTOFKINID", 420); //check field type
//$NEXTOFKINID = $xml->createElement("NEXTOFKINID", 30522);//modify for NOK working
$NextOfKinRecord -> appendChild($NEXTOFKINID);

//$RECORDSTATUS = $xml->createElement("RECORDSTATUS", 1); //check field type
$RECORDSTATUS = $xml->createElement("RECORDSTATUS");
$NextOfKinRecord -> appendChild($RECORDSTATUS);

$RELATIONSHIP = $xml->createElement("RELATIONSHIP");
$NextOfKinRecord -> appendChild($RELATIONSHIP);

//$CREATED = $xml->createElement("CREATED", "16/08/1991"); //check field type
$CREATED = $xml->createElement("CREATED", date("d/m/Y",time()));//modify for NOK working
$NextOfKinRecord -> appendChild($CREATED);

//$CREATEDBY = $xml->createElement("CREATEDBY", 11);//check field type
$CREATEDBY = $xml->createElement("CREATEDBY", "NIL");
$NextOfKinRecord -> appendChild($CREATEDBY);

$UPDATED = $xml->createElement("UPDATED", "NIL"); //check field type
$NextOfKinRecord -> appendChild($UPDATED);

$UPDATEDBY = $xml->createElement("UPDATEDBY", "NIL"); //check field type
$NextOfKinRecord -> appendChild($UPDATEDBY);

$TYPE = $xml->createElement("TYPE", 1); //check field type
$NextOfKinRecord -> appendChild($TYPE);

echo $xml->saveXML();
?>