<?php
include('../includes/configure.php');
include("includes/session_check.php");
$getQry="select * from tbl_settings where location='".$_SESSION["location"]."'";
$getRes=mysql_query($getQry);
$getRow=mysql_fetch_array($getRes);
$State=stripslashes($getRow["queue_system"]);
if(isset($_POST['hidden'])){            
  $state=trim($_POST['hidden']);
  $updateQry="update tbl_settings set queue_system='".$state."',modified_date='$dbdatetime' where location='".$_SESSION["location"]."'";
  $updateRes=mysql_query($updateQry);
	if($updateRes){
		header("Location:settings.php");
		exit;
	}
}
include('includes/header.php');
?>
<!-- Form Validation -->
<script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
<script type="text/javascript" src="../plugins/validation/additional-methods.min.js"></script>
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

<!-- Smartphone Touch Events -->
<script type="text/javascript" src="../plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="../plugins/event.swipe/jquery.event.move.js"></script>
<script type="text/javascript" src="../plugins/event.swipe/jquery.event.swipe.js"></script>

<!-- General -->
<script type="text/javascript" src="../assets/js/libs/breakpoints.js"></script>
<script type="text/javascript" src="../plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
<script type="text/javascript" src="../plugins/cookie/jquery.cookie.min.js"></script>
<script type="text/javascript" src="../plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="../plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

<!-- Page specific plugins -->
<!-- Charts -->
<script type="text/javascript" src="../plugins/sparkline/jquery.sparkline.min.js"></script>

<script type="text/javascript" src="../plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="../plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="../plugins/blockui/jquery.blockUI.min.js"></script>

<!-- Forms -->
<script type="text/javascript" src="../plugins/typeahead/typeahead.min.js"></script> <!-- AutoComplete -->
<script type="text/javascript" src="../plugins/autosize/jquery.autosize.min.js"></script>
<script type="text/javascript" src="../plugins/inputlimiter/jquery.inputlimiter.min.js"></script>
<script type="text/javascript" src="../plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
<script type="text/javascript" src="../plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="../plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->
<script type="text/javascript" src="../plugins/fileinput/fileinput.js"></script>
<script type="text/javascript" src="../plugins/duallistbox/jquery.duallistbox.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>
<script type="text/javascript" src="../plugins/bootstrap-switch/bootstrap-switch.js"></script>
<!-- Globalize -->
<script type="text/javascript" src="../plugins/globalize/globalize.js"></script>
<script type="text/javascript" src="../plugins/globalize/cultures/globalize.culture.de-DE.js"></script>
<script type="text/javascript" src="../plugins/globalize/cultures/globalize.culture.ja-JP.js"></script>
<div id="content">
			<div class="container">				
				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Change Settings</h3>
						<!-- <span>Add/Edit Doctor</span> -->
					</div>
				
				</div>
				<!-- /Page Header -->
				<?php if($msg!=''){
				?>
				<div class="alert fade in alert-<?php echo $class;?>">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php echo $Message; ?>
				</div>					
				<?php }
				?>
				<!--=== Page Content ===-->
				<!--=== Full Size Inputs ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Queue System Settings</h4>
								 <div class="toolbar no-padding">
									
								</div> 
							</div>
							<div class="widget-content">
							<form class="form-horizontal row-border"  method="post" name="edit_form" id="edit_form">
                                        <INPUT TYPE="hidden" NAME="hidden" id="hidden">
									<div class="form-group">
										<label class="col-md-4 control-label">ON/OFF Queue System</label>
										<div class="col-md-8">
											<div>
												<div class="make-switch switch-large" >
													<input type="checkbox" class="toggle" <?php if($State==0){echo checked;
								}?>>
												</div>
												
											</div>
										</div>
										
									</div>
									 
							  </div>
							</form>
							
					</div>
						</div>
					</div>
				</div>				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
		
<?php
include("includes/footer.php");
?>
