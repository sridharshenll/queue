<?php
include('../includes/configure.php');
include("includes/session_check.php");
if(isset($_POST["page_val"]) || isset($_POST["page_title"])){
	$page_val=	(!empty($_POST["page_val"]))?$_POST["page_val"]:$_POST["page_title"];
	$getpatientQry="select * from tbl_content where cnt_pagename='".$page_val."'";
	$getpatientRes=mysql_query($getpatientQry);
	$getpatientRow=mysql_fetch_array($getpatientRes);
	$cnt_banimage_db=stripslashes($getpatientRow["cnt_banimage"]);
	$cnt_content_db=stripslashes($getpatientRow["cnt_content"]);
	$cnt_id=$getpatientRow["cnt_id"];
}
if(isset($_POST["page_title"]) && empty($_POST["page_val"])){
   $page=addslashes(trim($_POST["page_title"]));
   $banner_image_filename=pathinfo($_FILES["banner_image"]["name"], PATHINFO_FILENAME);
   $banner_image_extension=pathinfo($_FILES["banner_image"]["name"], PATHINFO_EXTENSION);
   if($banner_image_filename!=""){
      $banner_image_filename=pathinfo($_FILES["banner_image"]["name"], PATHINFO_FILENAME);
	  $banner_image_extension=pathinfo($_FILES["banner_image"]["name"], PATHINFO_EXTENSION);
	  $renamed_banner_image=time().".".$banner_image_extension;
      $banner_image=$renamed_banner_image;
      $banner_image=$renamed_banner_image;
	  $renamed_banner_image_path=MOVE_BANNER_PATH.$renamed_banner_image;
	  move_uploaded_file($_FILES["banner_image"]["tmp_name"],$renamed_banner_image_path);

  }
  else{
     $banner_image=$cnt_banimage_db;
   }
 
 
	$content=addslashes(trim($_POST["content"]));
	$updateQry="update tbl_content set 
	cnt_banimage='".$banner_image."',cnt_content='".$content."',cnt_moddate=now() where cnt_pagename='".$page."'";
	$updateRes=mysql_query($updateQry);
	$getpatientQry="select * from tbl_content where cnt_pagename='".$page."'";
	$getpatientRes=mysql_query($getpatientQry);
	$getpatientRow=mysql_fetch_array($getpatientRes);
	$cnt_banimage_db=stripslashes($getpatientRow["cnt_banimage"]);
	$cnt_content_db=stripslashes($getpatientRow["cnt_content"]);
	$cnt_id=$getpatientRow["cnt_id"];

	$file = 'update.txt';
// Open the file to get existing content
$current = file_get_contents($file);
// Append a new person to the file
$current .= $updateQry;
// Write the contents back to the file
file_put_contents($file, $current);
}
include('includes/header.php');
?>
<script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
<script>
        tinymce.init({
			selector:'textarea',
			 plugins: [
                "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking"
              
            ]
			
			
			});
</script>
<div id="content">
			<div class="container">				
				<!--=== Page Header ===-->
				<div class="page-header" style="margin-top:60px;">
					<div class="page-title">
						
					</div>					
				</div>
				<!-- /Page Header -->

				<?php if($msg!=''){
				?>
				<div class="alert fade in alert-success">
					<i class="icon-remove close" data-dismiss="alert"></i>
					<?php echo $Message; ?>
				</div>					
				<?php }
				?>

				<!--=== Page Content ===-->
				<!--=== Full Size Inputs ===-->
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i>Edit Content</h4>
								
							</div>
							<div class="widget-content">
								<form class="form-horizontal row-border" method='POST'  name="edit_content" id="edit_content"  enctype="multipart/form-data">
								<!-- <div class="form-group" style="margin-top:-30px;">
									<input type="hidden" name="page_val" id="page_val" >
								</div> -->
								
										<div class="form-group">
														<input type="hidden" name="page_val" id="page_val" >
														<label class="control-label col-md-2">Select Page <span class="required">*</span></label>
														<div class="col-md-8">
															<select name="page_title" id="page_title" class="full-width-fix required" onchange="setvalue(this.value)">
																<option value="">Select Page</option> 
																<option value="home">Home</option>
																<option value="patient_info">Information For Patients</option>
																<option value="emp_offer">Employment Opportunity</option>
																<option value="special_offer">Specialist Referral Services</option>
																<option value="open_hours">Opening Hours</option>
																<option value="Contact">Contact Us</option>
																<option value="faqs">FAQs</option>
																<option value="links">Links</option>
																
															</select>
															<?php
															$sel_val=(!empty($page))?$page:$page_val;
															?>
															<script language="javascript">
																$("#page_title").val("<?php echo $sel_val;?>");
															</script>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-2">Banner<span class="required"></span></label>
														<div class="col-md-8">
															<input type="file" name="banner_image" id="banner_image">
                                                            <span class="help-block">(Banner image size should be 743x216).</span>
														<?php
														if($cnt_banimage_db!=""){
														?>
														<br>
														<img src="<?php echo MOVE_BANNER_PATH.$cnt_banimage_db;?>" style="width:75px;height:75px;">
														<?php
														}
														?>
														</div>
                   
													</div>
													<div class="form-group">
														<label class="control-label col-md-2">Content <span class="required">*</span></label>
														<div class="col-md-8">
															 <textarea name="content" id="cont_desc" rows="25"  cols="25"><?php echo $cnt_content_db;?></textarea> 
															
														</div>
													</div>
												<div class="form-actions">
									
										<input type="button" value="Save" class="btn btn-primary pull-right" id="submit_btn">
									</div>
									
								</form>
							</div>
						</div>
					</div>
				</div>				
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
	
		
<?php
include("includes/footer.php");
?>
<script>
$(document).ready(function(){
 $("#submit_btn").click(function(){	
	if($.trim($("#page_title").val())==""){
		alert("Please select page.");
		$("#page_title").focus();
		return false;
	}
	else if($.trim(tinyMCE.get('cont_desc').getContent())==""){
			 alert("Please enter description.");
		     tinymce.execCommand('mceFocus',false,'cont_desc');
			 return false;
	}
	else{
      $("#page_val").val("");	
	  $("#edit_content").submit();
	}
});


});
function setvalue(val){
	$("#page_val").val(val);
	$("#edit_content").submit();
	
}
</script>
