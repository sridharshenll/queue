<?php
include("includes/configure.php");
include("insert_data.php");
include("includes/header.php");
?>
<style>
#content{
	background: #fff;
	margin-left: 0px;
	 overflow: visible;
	padding-bottom: 30px;
	min-height: 100%;
}
.navbar .container .navbar-brand {
	display:block;
}
label{
	white-space:nowrap;
}

.inputbx {color: #555;padding: 6px !important;font-weight: normal;vertical-align: top;background-color: #FFF;border-color: #4D7496;box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075), 0px 0px 3px rgba(0, 0, 0, 0.1);width: 100%};

@media (min-width:993px)and max-width:1050px){
.form-body>ul>li>a{
	border:1px solid green;
	width:20%;
}
}




</style>

<script language="javascript">
function validateHCC(clickfrm)
{
var pattern = /[\d]{9}[A-Za-z]{1}/g;
var str = document.getElementById("patient_hcc").value;

if(str!="")
	{
if (pattern.test(str)) {
document.getElementById("patienthcclbl").style.display="none";
if(clickfrm=="submit")
	{
	document.getElementById("queue_system").submit();
}
else
	{
	return false;
	}
//	i=0;
//	var numarray = new Array();
//	var chararray = new Array();
//	while(i<str.length)
//	{
//		if(isNaN(str[i]))
//			chararray = str[i];
//		else 
//			numarray = numarray + ""+str[i];
//		i++;
//	}
//	if(numarray.length!=9)
//	{
//		alert("Centrelink HCC/Pension Card No should have 9 numbers.");
//		//return false;
//	}
//  } 
//  else
//  {
//	  alert("Centrelink HCC/Pension Card No should have alteast one alphabet.");
//		//return false;
  }
  else
	{
	 //alert("Centrelink HCC/Pension Card No should have 9 numbers following by 1 alphabet.");
	  document.getElementById("patienthcclbl").style.display="";
	}
	}
}
</script>
<style>
@media screen and (max-width:1024px) and (orientation:landscape) {.nav-justified{display:inline !important;text-align:left;} 
.nav>li{position: relative;display: block;}}

	
</style>
		<div id="content">
			<div class="container">
				

				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Patients Queue</h3>
						<!-- <span>Good morning, John!</span> -->
					</div>

					
				</div>
				<!-- /Page Header -->

				<!--=== Page Content ===-->
				<div class="row">
					<!--=== Form Wizard ===-->
					<div class="col-md-12">
						<div class="widget box" id="form_wizard">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i> Queue System - <span class="step-title">Step 1 of 5</span></h4>
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							<div class="widget-content">
								<form class="form-horizontal" id="queue_system" name="queue_system" method="post">
								<input type="hidden" id="queue_sytem_set">
								<input type="hidden" id="queue_sytem_loc">
									<div class="form-wizard">
										<div class="form-body">
												
											<!--=== Steps ===-->
											<ul class="nav nav-pills nav-justified steps" style="width: 100%;">
												<li>
													<a href="#tab1" data-toggle="tab" class="step" >
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Welcome</span>
													</a>
												</li>
												<li>
													<a href="#tab2" data-toggle="tab" class="step">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i>Information</span>
													</a>
												</li>
												<li>
													<a href="#tab3" data-toggle="tab" class="step">
														<span class="number">3</span>
														<span class="desc"><i class="icon-ok"></i> Location</span>
													</a>
												</li>
												<li>
													<a href="#tab4" data-toggle="tab" class="step">
														<span class="number">4</span>
														<span class="desc"><i class="icon-ok"></i> Status</span>
													</a>
												</li>
												<li>
													<a href="#tab5" data-toggle="tab" class="step">
														<span class="number">5</span>
														<span class="desc"><i class="icon-ok"></i> Details</span>
													</a>
												</li>
											</ul>
											<!-- /Steps -->

											<!--=== Progressbar ===-->
											<div id="bar" class="progress progress-striped" role="progressbar">
												<div class="progress-bar progress-bar-success"></div>
											</div>
											<!-- /Progressbar -->

											<!--=== Tab Content ===-->
											<div class="tab-content">

												<!--=== Available On All Tabs ===-->
												<!-- <div class="alert alert-danger hide-default">
													<button class="close" data-dismiss="alert"></button>
													You missed some fields. They have been highlighted.
												</div> -->
												
												<!-- /Available On All Tabs -->

												<!--=== Basic Information ===-->
												<div class="tab-pane active" id="tab1">
													<h3 class="block padding-bottom-10px">You can use the REAL TIME queue system if you:</h3>
														<ol>
															<li>Have a valid Medicare card; and</li>
															<li>Need to see the doctor now. This is not an appointment system for any other times; and</li>
															<li>Can get to the clinic immediately; and</li>
															<li>Have a non life threatening medical condition (please ring 000 if you need emergency medical cares).</li>
															 
														</ol>
														<p>
															Please note that if you are not in the waiting room when your name is called, you will lose your position in the queue and will need to re-queue again. 
														</p>
														<p>
															Non Medicare related consultations (pre-employment, OSHC, driver licence medical etc) are not able to use this system. Please proceed to the clinic. 

														</p>
														<h3><center>Current Queue in <span style="color:blue;"><a href="patients_list.php?loc=Burswood" style="text-decoration:underline;">Burswood</a> </span>and  <span  style="color:red;"><a href="patients_list.php?loc=Claremont" style="text-decoration:underline;">Claremont</a> </span></center></h3>
													
												</div>
												<!-- /Basic Information -->

												<!--=== Your Profile ===-->
												<div class="tab-pane" id="tab2">
													<div class="alert alert-danger hide-default">
														<button class="close" data-dismiss="alert"></button>
														Please accept the Terms to continue
													</div>
													<h3 class="block padding-bottom-10px">The doctors do not provide the following services:</h3>

													
													<ul>
														<li>Prescribing of any drugs of addiction, narcotics or sleeping tablets.</li>
														<li>Medical legal report (car accident, domestic violence, work injury, insurance).</li>
														<li>Report writing of any Centrelink form.</li>
														<li>Report writing of any insurance form.</li>
														<li>Endorsement of Immunisation Exemption Conscientious Objection form</li>
														<li>Back dating of a medical certificate.</li>
														<li>Referral for termination of pregnancy.</li>
													</ul>
														<div class="form-group">
															<div class="col-md-8">
																<div class="radio-list">
																		<label>
																			<input type="checkbox" name="agree_service" value="yes" data-title="Yes, I agree" class="required" data-msg-required="">
																			&nbsp;&nbsp;Yes, I agree
																		</label>
																		
																	</div>
																	<label for="agree_service" class="has-error help-block" generated="true" style="display:none;"></label>
															</div>
														</div>
															
													</div>

												<!-- /Your Profile -->

												<!--=== Billing Setup ===-->
												<div class="tab-pane" id="tab3">
													<div class="alert alert-danger hide-default">
														<button class="close" data-dismiss="alert"></button>
														You missed some fields. They have been highlighted.
													</div>
													<h3 class="block padding-bottom-10px">I would like to see the doctor now at:</h3>

													<div class="form-group">
														
														<div class="col-md-8">
															<div class="radio-list">
																<label>
																	<input type="radio" name="clinic_location" value="Burswood" data-title="Burswood" class="required" onclick="displaySpan(this.value)" data-msg-required="Please select location." id="clinic_location" checked/>
																	Burswood
																</label>
																<label>
																	<input type="radio" name="clinic_location" value="Claremont" data-title="Claremont" onclick="displaySpan(this.value)"/>
																	Claremont
																</label>
																<script type="text/javascript">
																$(document).ready(function(){
																	$("#clinic_location").trigger('click');
																});
																</script>
															</div>
															<label for="clinic_location" class="has-error help-block" generated="true" style="display:none;" data-msg-required="Please select location."></label>
														</div>
													</div>
												</div>
												<!-- /Billing Setup -->

												<!--=== Confirmation ===-->
												<div class="tab-pane" id="tab4">
													<div class="alert alert-danger hide-default">
														<button class="close" data-dismiss="alert"></button>
														You missed some fields. They have been highlighted.
													</div>
													<h3 class="block padding-bottom-10px">Have you been to the <span id="location_span">Burswood</span> clinic?</h3>
													<div class="form-group">
														
														<div class="col-md-8">
															<div class="radio-list">
																<label>
																	<input type="radio" name="clinic_confirmation" value="Yes" data-title="Yes" onclick='set(this.value)' class="required" checked/>
																	Yes
																</label>
																<label>
																	<input type="radio" name="clinic_confirmation" onclick='set(this.value)' value="No" data-title="No"/>
																	No
																</label>
															</div>
															<label for="clinic_confirmation" class="has-error help-block" generated="true" style="display:none;"></label>
														</div>
													</div>
													
												</div>
												<!-- /Confirmation -->
												<div class="tab-pane" id="tab5">
													<div class="alert alert-danger hide-default">
														<button class="close" data-dismiss="alert"></button>
														You missed some fields. They have been highlighted.
													</div>
													<h3 class="block padding-bottom-10px">Please enter your personal details below:</h3>
													<div class="form-group">
														<label class="control-label col-md-3">Title: <span class="required">*</span></label>
														<div class="col-md-4">
															<select name="patient_title" id="patient_title" class="required" data-msg-required="Please select title.">
																<option value="">Select Title</option>
																<option value="Master">Master</option>
																<option value="Mr">Mr</option>
																<option value="Mrs">Mrs</option>
																<option value="Ms">Ms</option>
																<option value="Miss">Miss</option>
															</select>
															<!-- <span class="help-block">Please select title.</span> -->
															</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Family Name: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required letterswithbasicpunc" name="family_name" id="family_name" data-msg-required="Please enter family name."/>
															<span class="help-block">(as printed on your medicare card).</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Given Names: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required letterswithbasicpunc" name="given_names" id="given_names" data-msg-required="Please enter given names."/>
															<span class="help-block">(as printed on your medicare card).</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Date of Birth: <span class="required">*</span></label>
														<div class="col-md-4"  style="padding-left:31px">
															<div class="form-group col-md-4" style="padding-bottom:5px;margin-right:15px">
																


																<select name="birth_date" id="birth_date" class="form-control required" data-msg-required="Select date." style="width:100px" data-rule-required="true">
																<option value="">Date</option>
																<?php
																	
																	for($d=1;$d<=31;$d++)
																	{
																		echo "<option value='".$d."'>".$d."</option>";	
																	}
																	?>																
																</select>
															</div>
															<div class="form-group col-md-4"  style="padding-bottom:5px;margin-right:15px">
																<select name="birth_month" id="birth_month1" class="form-control required" data-msg-required="Select month." style="width:100px"  data-rule-required="true">
																	<option value="">Month</option>
																	<option value="1">01</option>
																	<option value="2">02</option>
																	<option value="3">03</option>
																	<option value="4">04</option>
																	<option value="5">05</option>
																	<option value="6">06</option>
																	<option value="7">07</option>
																	<option value="8">08</option>
																	<option value="9">09</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
																	
																</select>
															</div>
															<div class="form-group col-md-4"  style="padding-bottom:5px;margin-right:15px">
																<select name="birth_year" id="birth_year" class="form-control required" data-msg-required="Select year." style="width:100px" data-rule-required="true">
																<option value="">Year</option>
																<?php
																	
																	for($y=1900;$y<=2030;$y++)
																	{
																		echo "<option value='".$y."'>".$y."</option>";	
																	}
																	?>																
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Medicare Number: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control required medicare integer" name="patient_medicareno" id="patient_medicareno" maxlength="10" onblur="auto_fill(this.value)" data-msg-required="Please enter medicare number."/>
															<!-- <span class="help-block">Enter Medicare Number.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Reference Number: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control referno integer" name="patient_referno" id="patient_referno" maxlength="1" data-msg-required="Please enter reference number."/>
															<span class="help-block">(the number in front of your name on the card. eg 1, 2, 3 etc)</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Expiry Date: <span class="required">*</span></label>
														<div class="col-md-4" style="padding-left:31px;width:360px;">	
															<div class="col-md-4 form-group "  style="padding-bottom:5px;margin-right:15px">
																<select name="expiry_month_medi" id="expiry_month_medi" class="form-control required " data-msg-required="Select month." style="width:100px">
																	<option value="">Month</option>
																	<option value="01">01</option>
																	<option value="02">02</option>
																	<option value="03">03</option>
																	<option value="04">04</option>
																	<option value="05">05</option>
																	<option value="06">06</option>
																	<option value="07">07</option>
																	<option value="08">08</option>
																	<option value="09">09</option>
																	<option value="10">10</option>
																	<option value="11">11</option>
																	<option value="12">12</option>
																</select>
															</div>
															<div class="col-md-4 form-group "  style="padding-bottom:5px;margin-right:15px">

															<select name="expiry_year_medi" id="expiry_year_medi" class="form-control required " data-msg-required="Select year." style="width:100px">
																<option value="">Year</option>
																<?php
																	
																	for($y=2013;$y<=2050;$y++)
																	{
																		echo "<option value='".$y."'>".$y."</option>";	
																	}
																	?>
															</select>
															</div>
															
														</div>
														
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">I prefer to see: <span class="required">*</span></label>
														<div class="col-md-4">
															<select name="preffered_doctor" id="preffered_doctor" class="form-control required " data-msg-required="Please select doctor.">
																<option value=""></option>
																<option value="Mr">Mr</option>
																<option value="Mrs">Mrs</option>
																<option value="Ms">Ms</option>
																<option value="Miss">Miss</option>
															</select>
															<!-- <span class="help-block">Please select prefered doctor.</span> -->
														</div>
													</div>
													<div class="form-group" id="Other">
														<label class="control-label col-md-3">&nbsp;</label>
														<div class="col-md-4" style="border-top-style:solid;color:green;width:75%;">
															(only fill in below if new  details since your last  visit)
														</div>
														
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Address:<span class="required">*</span>
														<!-- <span class="required">*</span> --></label>
														<div class="col-md-4">
															<textarea class="form-control " rows="3" name="patient_address" id="patient_address"  data-msg-required="Please enter address." ></textarea>
															<!-- <span class="help-block">Enter address.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Suburb: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control letterswithbasicpunc  " name="patient_suburb" id="patient_suburb"  data-msg-required="Please enter suburb." />
															<!-- <span class="help-block">Enter suburb.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Postcode: <span class="required">*</span></label>
														<div class="col-md-4">
															<input type="text" class="form-control postcode integer  " name="patient_postcode" id="patient_postcode" data-msg-required="Please enter postcode." maxlength="4" data-mask="9999">
															<!-- <span class="help-block">Enter postcode.</span> -->
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Home Phone: </label>
														<div class="col-md-4">
															<input type="text" class="form-control homephone integer  " name="patient_homephone" id="patient_homephone" maxlength="10" />
															
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Mobile Phone: <span class="required">*</span></label><!-- medicare integer -->
														<div class="col-md-4">
															<input type="text" class="form-control mobile" name="patient_mobile" id="patient_mobile" maxlength="10" data-msg-required="Please enter mobile phone." >
															
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Centrelink HCC/Pension Card No: </label>
														<div class="col-md-4">
															<input type="text" class="form-control pension" name="patient_hcc" id="patient_hcc" maxlength="10"  />
															<label id="patienthcclbl" class="has-error" for="patient_hcc" style="color:red;display:;">Please enter valid pension card number.</label>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Expiry Date: </label>
														<div class="col-md-4" style="padding-left:31px;width:360px;">
																<div class="col-md-4 form-group "  style="padding-bottom:5px;margin-right:15px">
																	<select name="hcc_ref_expiry_date" id="hcc_ref_expiry_date" class="form-control"  data-msg-required="Select date." style="width:100px">
																	<option value="">Date</option>
																	<?php
																		for($hccd=1;$hccd<=31;$hccd++)
																		{
																			if(strlen($hccd)==1)
																				$hccd="0".$hccd;
																			echo "<option value='".$hccd."'>".$hccd."</option>";	
																		}
																		?>
																	</select>
																</div>
																<div class="col-md-4 form-group "  style="padding-bottom:5px;margin-right:15px">
																	<select name="hcc_ref_expiry_month" id="hcc_ref_expiry_month" class="form-control"  data-msg-required="Select month." style="width:100px">
																		<option value="">Month</option>
																		<?php
																		for($hccm=1;$hccm<=12;$hccm++)
																		{
																			if(strlen($hccm)==1)
																				$hccm="0".$hccm;
																			echo "<option value='".$hccm."'>".$hccm."</option>";	
																		}
																		?>
																	</select>
																</div>
																<div class="col-md-4 form-group "  style="padding-bottom:5px">
																
															
																<select name="hcc_ref_expiry_year" id="hcc_ref_expiry_year" class="form-control"   data-msg-required="Select year." style="width:100px">
																<option value="">Year</option>
																<?php
																	
																	for($y=2013;$y<=2050;$y++)
																	{
																		echo "<option value='".$y."'>".$y."</option>";	
																	}
																	?>
																</select>
																</div>
															</div>
															
														</div>
													</div>
												</div>

												
											</div>
											
											</div>
											<!-- /Tab Content -->
										</div>

										<!--=== Form Actions ===-->
										<div class="form-actions fluid">
											<div class="row">
												<div class="col-md-12">
													<div class="col-md-offset-3 col-md-9">
														<a href="javascript:void(0);" class="btn button-previous">
															<i class="icon-angle-left"></i> Back
														</a>
														<a href="javascript:void(0);" class="btn btn-primary button-next">
															Continue <i class="icon-angle-right"></i>
														</a>
														<a href="javascript:void(0);" class="btn btn-success button-submit">
															Submit <i class="icon-angle-right"></i>
														</a>
													</div>
												</div>
											</div>
										</div>
										<script>
													function set(val){
														//window.reload();
														if(val=="No"){
                                                          document.getElementById("Other").style.display="none";
														    $('#patient_address').addClass('required');
															$('#patient_suburb').addClass('required');
															$('#patient_postcode').addClass('required');
//															$('#patient_hcc').addClass('required');
//															$('#hcc_ref_expiry_month').addClass('required');
//															$('#hcc_ref_expiry_date').addClass('required');
//															$('#hcc_ref_expiry_year').addClass('required');
                                                            $('#patient_mobile').addClass('required integer mobile');
														}
														else{
															
															document.getElementById("Other").style.display="block";
															$('#patient_address').removeClass(' required');
															$('#patient_suburb').removeClass('required');
															$('#patient_postcode').removeClass('required');
															$('#patient_mobile').removeClass('required');
//															$('#patient_hcc').removeClass('required');	
															}
														}
														
													</script>
										<!-- /Form Actions -->
									</div>
								</form>
							</div>
						</div>
						<!-- /Form Wizard -->
					</div>
				<!-- /Page Content -->
			</div>
			<!-- /.container -->

		</div>
		<script>
			$("#patient_mobile").click(function(e){
					if($(this).val()!=""){
						$('#patient_mobile').addClass('required integer');
					}
				
			});

				$("#patient_hcc").change(function(e){
					if($(this).val()!=""){
				$('#hcc_ref_expiry_month').addClass('required');
				$('#hcc_ref_expiry_date').addClass('required');
				$('#hcc_ref_expiry_year').addClass('required');
					}
					else
					{
					$('#hcc_ref_expiry_month').removeClass('required');
					$('#hcc_ref_expiry_date').removeClass('required');
					$('#hcc_ref_expiry_year').removeClass('required');
					}
				
			});

		</script>
<?php
include("includes/footer.php");
?>


